#######################################################
Création et mise en oeuvre des événements et des rappels
#######################################################

Création de gestionnaires d'événements ; abonnement aux événements et désabonnement ; utilisation de types délégués intégrés pour créer des événements ; création de délégués ; expressions lambda ; méthodes anonymes

