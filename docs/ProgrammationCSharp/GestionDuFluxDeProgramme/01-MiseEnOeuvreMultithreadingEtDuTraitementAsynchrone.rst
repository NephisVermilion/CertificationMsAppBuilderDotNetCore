###########################################################
Mise en oeuvre du multithreading et du traitement asynchrone
###########################################################

Utilisation de la bibliothèque Task Parallel notamment theParallel.For method, PLINQ, Tasks ; création de tâches de continuation ; génération dynamique des threads à l'aide de ThreadPool ; déblocage de l'interface utilisateur ; utilisation des mots clés async et await ; gestion des données à l'aide des collections simultanées
