#########################
Gestion du multithreading
#########################

Synchronisation des ressources ; mise en œuvre du verrouillage ; annulation d'une tâche à long terme ; mise en œuvre de méthodes thread-safe pour gérer les conditions de concurrence critique

