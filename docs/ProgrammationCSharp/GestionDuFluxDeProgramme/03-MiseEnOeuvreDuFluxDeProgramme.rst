##################################
Mise en oeuvre du flux de programme
##################################

Itération à travers les éléments de collecte et de tableau ; programmation des décisions à l'aide d'instructions switch, si/alors et d'opérateurs ; évaluation des expressions
