##########################################
Mise en oeuvre de la gestion des exceptions
##########################################

Gérer des types d'exception, y compris les exceptions SQL, les exceptions réseau, les exceptions de communication, les exceptions de dépassement de délai réseau ; utiliser des déclarations de captures ; utiliser la classe de base d'une exception ; implémenter des blocs try-catchfinally ; jeter des exceptions ; renvoyer une exception ; créer des exceptions personnalisées ; gérer les exceptions internes ; gérer l'exception globale
