##########################
Examen 70-483/Cours 20483B
##########################

Programmation en C#
=============================================

Les candidats à cet examen doivent avoir une ou plusieurs années d'expérience dans la programmation de la logique essentielle des affaires / les applications pour une variété de types d'applications et de plates-formes matérielles / logicielles utilisant C #.

..  toctree::
    :maxdepth: 4
    :numbered:

    CréationEtUtilisationDesTypes/index
    DéboguageDApplicationEtMiseEnOeuvreDeLaSécurité/index
    GestionDuFluxDeProgramme/index
    MiseEnOeuvreDeLAccèsAuxDonnées/index
