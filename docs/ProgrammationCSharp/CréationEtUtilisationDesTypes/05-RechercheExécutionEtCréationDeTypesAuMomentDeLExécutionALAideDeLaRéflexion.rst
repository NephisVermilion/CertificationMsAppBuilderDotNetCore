###########################################################################################
Recherche, exécution et création de types au moment de l'exécution à l'aide de la réflexion
###########################################################################################

Création et application d'attributs ; lecture d'attributs ; génération de code au moment de l'exécution à l'aide des expressions CodeDom et lambda ; utilisation de types à partir de l'espace de noms System.Reflection (Assembly, PropertyInfo, MethodInfo, Type) 
