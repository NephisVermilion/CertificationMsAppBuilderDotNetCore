##############################
Application de l'encapsulation
##############################

Appliquer l'encapsulation en utilisant les propriétés ; appliquer l'encapsulation en utilisant des accesseurs qu’ils soient publics, privés, protégés et internes ; appliquer l'encapsulation en utilisant une implémentation d'interface explicite

=====================================================
Appliquer l'encapsulation en utilisant les propriétés
=====================================================

Une propriété est l'un des éléments contenu dans un objet, l'encapsulation permet de créer des règles de lecture, et écriture sur ces propriétés. Dans l'exemple ci-dessous, la propriété est en lecture/écriture, publique et, lors de la modification de la propriétés de type string ou lors de sa lecture, celle-ci est automatiquement mise en majuscule.

.. code-block:: csharp

    class SampleClass
    {
        private string _myString;
        public string MyString
        {
            // Return the value stored in a field.  
            get { return _myString.ToUpper(); }
            // Store the value in the field.  
            set { _myString = value.ToUpper(); }
        }
    }
======================
Utiliser les indexeurs 
======================

Les indexeurs permettent de fournir un moyen d'accès au données des éléments d'un tableau ou d'une collection.

par exemple le type "string" contient un indexeur sur le type "char"

.. code-block:: csharp

    string hello = "Hello";
    char c = hello[2];

Dans l'exemple ci-dessous, on utilise l'indexeur sur le tableau temps de classe tempRecord pour retourner la valeur du tableau qui est supossé en degré fahrenheit en degré celcius.

.. code-block:: csharp
    
   
    class TempRecord
    {
        // Array of temperature values
        private float[] temps = new float[10] { 56.2F, 56.7F, 56.5F, 56.9F, 58.8F,
                                                61.3F, 65.9F, 62.1F, 59.2F, 57.5F };
        // To enable client code to validate input 
        // when accessing your indexer.
        public int Length
        {
            get { return temps.Length; }
        }
        // Indexer declaration.
        // If index is out of range, the temps array will throw the exception.
        public float this[int index]
        {
            get
            {
            var result = (temps[index] - 32) / 1.8F;
            return result ;
            }
            set
            {
                temps[index] = value;
            }
        }
    }
    class MainClass
    {
        static void Main()
        {
            TempRecord tempRecord = new TempRecord();
            // Use the indexer's set accessor
            tempRecord[3] = 58.3F;
            tempRecord[5] = 60.1F;
            // Use the indexer's get accessor
            for (int i = 0; i < 10; i++)
            {
                System.Console.WriteLine("Element #{0} On celcius = {1}", i, tempRecord[i]);
            }
            System.Console.WriteLine("Lenght of array :" + tempRecord.Length);
            System.Console.ReadLine();
        }
    }

