####################################################
Création et mise en œuvre d'une hiérarchie de classe
####################################################

Conception et mise en œuvre d'une interface ; héritage d'une classe de base ; création et mise en œuvre de classes en fonction des interfaces IComparable, IEnumerable, IDisposable et IUnknown

=========
Interface
=========

Une interface contient uniquement des signatures de méthodes, de propriétés, d’événements ou d’indexeurs. Une classe ou une structure qui implémente l’interface doit implémenter les membres de l’interface qui sont spécifiés dans la définition de l’interface. Dans l’exemple suivant, la classe MyBeautifull doit implémenter une méthode nommée MyFirst() qui n’a aucun paramètre et qui retourne void, sinon une erreur lors de la compilation apparaitra. On peut voir par contre que dans la classe on à ajouter une méthode MyThird() qui n'est pas dans les signatures de l'interface, et pourtant aucune erreur apparait, on peut rajouter des méthodes dans la classe, en plus des méthodes obligatoires.

Si par contre on déclare ensuite une instance de l'interface, elle même reliée à la classe, on ne pourra pas dans l'instance appelé la méthode myThird() ; elle fait partie de la classe mais pas de l'interface, si on veut accéder à la méthode myThird() il faut donc directement créer une instance de classe.

.. code-block:: csharp


    class MainClass
    {
        static void Main()
        {
            System.Console.ReadLine();
            IMyBeautifull myBeautifull = new MyBeautifull();
            var myProperty = myBeautifull.myString;
            myBeautifull.MyFirst();
            //No definition for this method on interface so, it's not work
            //myBeautifull.MyThird();

        }
        
    }
    public interface IMyBeautifull
    {
        string myString { get; set; }
        void MyFirst();
        bool MySecond(string myString);
    }
    public class MyBeautifull : IMyBeautifull
    {
        public string myString { get; set; }
        public void MyFirst()
        {
        
        }
        public bool MySecond(string myString)
        {
            return true;
        }
        public bool MyThird(float myFloat = 3.5F)
        {
            return false;
        }
    }

=============================
Héritage d'une classe de base 
=============================

L'héritage implique qu'un type dérive d'un type de base, dans notre cas notre classe hérite d'une autre classe, la classe dérivée hérite des fonctionnalités de la classe parente.

Par défault une classe hérite de **System.Object** implicitement.

Dans l'exemple ci-dessous, notre berline hérite de voiture, en plus pour notre berline nous avons ajouté la notions de longueur. Comme on peut le voir, si on initialise une berline, on est obligé de renseigner les paramètres d'une voiture et celui de la berline.


.. code-block:: csharp

using System;
    public class Car
    {
        public int nbDoor { get; set; }
        public int annee { get; set; }
        public string marque { get; set; }
        public Car()
        {
        }
        public Car(int nbDoor, int annee, string marque)
        {
            this.nbDoor = nbDoor;
            this.annee = annee;
            this.marque = marque;
        }
    }
    public class Saloon : Car
    {
        public float carLenght { get; set; }
        public Saloon(float carLenght,int nbdoor,int annee, string marque) : base (nbdoor, annee, marque)
        {
            this.carLenght = carLenght;
        }
    }
    class Program
    {
        static void Main()
        {
            // Call the constructor that has no parameters.
            Car car = new Car(5, 2100, "Tesla");
            Console.WriteLine("Car 1, marque : " + car.marque + " année : " + car.annee + " nombre de portes :" + car.nbDoor);
            Saloon Saloon = new Saloon(4.2F, 5, 2003, "Lexus");
            Console.WriteLine("Saloon 1, marque : " + saloon.marque + " année : " + saloon.annee + " nombre de portes :" + saloon.nbDoor + " longueur " + saloon.carLenght);
            Console.ReadLine();
        }
    }

=============================
Méthodes, propriétés virtuelles
=============================

Le faite de déclarer une méthode ou une propriété en virtuelle, permet de pouvoir autoriser dans une classe enfant de réimplémenté la méthode, si la méthode est réimplémenté, alors c'est celle-ci qui est automatiquement utilisé sinon c'est celle du parent.

.. code-block:: csharp

 using System;

    class MyBaseClass
    {
        private string name;
        public virtual string Name
        {
            get
            {
                if (name != String.Empty && name != null)
                {
                    return name;
                }
                else
                {
                    return "string null or empty base class";
                }
            }
            set
            {

            }
        }
        public virtual int number { get; set; }
    }


    class MyDerivedClass : MyBaseClass
    {
        private string name;

        // Override auto-implemented property with ordinary property
        // to provide specialized accessor behavior.
        public override string Name
        {
            get
            {
                if (name != String.Empty && name != null)
                {
                    return name;
                }
                else
                {
                    return "string null or empty derived class";
                }
            }
            set
            {
                if (value != String.Empty)
                {
                    name = value;
                }
                else
                {
                    name = "Test override";
                }
            }
        }

    }

    class Program
    {
        static void Main()
        {
            MyDerivedClass myDerived = new MyDerivedClass();
            MyBaseClass myBase = new MyBaseClass();
            Console.WriteLine(myDerived.Name);
            Console.WriteLine(myBase.Name);
            Console.ReadLine();
        }
    }