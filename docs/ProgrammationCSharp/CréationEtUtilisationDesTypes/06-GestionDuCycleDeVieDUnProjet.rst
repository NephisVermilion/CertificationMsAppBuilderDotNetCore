##################################
Gestion du cycle de vie d'un objet
##################################

Gestion des ressources non gérées ; mise en œuvre de l'interface IDisposable, y compris l'interaction avec finalisation ; gestion de l'interface IDisposable à l'aide de l'instruction Using ; gestion des opérations de finalisation et récupération d'espace mémoire

=============================================
Libérer de la mémoire avec la méthode dispose
=============================================

Utilisez la méthode Dispose de l'interface IDisposable permet de  libérer explicitement les ressources non managées en association avec le garbage collector. Le consommateur d’un objet peut appeler cette méthode lorsque l’objet n’est plus nécessaire. 

.. code-block:: csharp

    using System;
    using System.IO;

    class Program
    {
        static void Main()
        {
            TextReader textReader = null;

            try
            {
                //lets aquire the resources here                
                textReader = new StreamReader(@"Files\test.txt");

                //do some operations using resources
                string stringFile = textReader.ReadToEnd();
                textReader.Dispose();

                Console.WriteLine(stringFile);
            }
            catch (Exception ex)
            {
                //Handle the exception here
            }
        }
    }


========================================
Utiliser finally avec la méthode dispose
========================================

On prèfere de manière générale utiliser la méthode dispose dans un finally de manière à être sur que l'instruction est bien éxécuter même si une exception à été levée auparavant.

.. code-block:: csharp

    using System;
    using System.IO;

    class Program
    {
        static void Main()
        {
            TextReader textReader = null;

            try
            {
                //lets aquire the resources here                
                textReader = new StreamReader(@"Files\test.txt");

                //do some operations using resources
                string stringFile = textReader.ReadToEnd();

                Console.WriteLine(stringFile);
            }
            catch (Exception ex)
            {
                //Handle the exception here
            }
            finally
            {
                //lets release the aquired resources here
                if (textReader != null)
                {
                    textReader.Dispose();
                }
            }
        }
    }

=========================================
Utiliser using pour libérer de la mémoire
=========================================

La meilleure méthode pour gérer la libération de mémoire non managée reste le using, cela permet de libérer automatiquement la mémoire une fois le using passé.

.. code-block:: csharp

    using System;
    using System.IO;

    class Program
    {
        static void Main()
        {
            TextReader textReader = null;
            //lets aquire the resources here                
            try
            {
                using (textReader = new StreamReader(@"Files\test.txt"))
                {

                    //do some operations using resources
                    string s = textReader.ReadToEnd();
                    Console.WriteLine(s);
                }
            }
            catch (Exception ex)
            {
                //Handle the exception here
            }
        }
    }
