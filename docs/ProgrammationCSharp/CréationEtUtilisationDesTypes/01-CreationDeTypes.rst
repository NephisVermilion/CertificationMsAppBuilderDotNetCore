#################
Création de types
#################

- Création de classe et structure ;
- Créer des types de valeur, y compris les structures et enum ;
- Créer des types de référence, des types génériques, des constructeurs, des variables statiques, des méthodes, des classes, des méthodes d'extension ;
- créer des paramètres optionnels et nommés ;
- créer des propriétés indexées ;
- créer des méthodes surchargées et contournées

----------------------------------------
=======
Classes
=======

Une classe est une construction qui vous permet de créer vos propres types personnalisés en regroupant des variables d’autres types, méthodes et événements. Une classe **s’apparente à un plan**. Elle définit les données et le comportement d’un type. On peut instancié une classe, et donc créer un object à partir de ce plan si cette classe n'est pas statique.

Dans l'exemple ci-dessous on créer une classe "Car" c'est notre plan, ce plan comporte 3 propriétés : le nombre de portes,l'année et la marque de la voiture. On peut créer des voiture à partir de ce plan, on parle **d'instance de classe**. Dans notre exemple on créer 2 voitures différentes et on utilise diverses méthodes pour instanciés notre classes, directement dans le constructeur, à l'aide d'une méthode annexe ect...

.. code-block:: csharp

    using System;

    public class Car
    {
        // Field
        public int nbDoor { get; set; }
        public int annee { get; set; }
        public string marque { get; set; }
        // Constructor that takes no arguments.
        public Car()
        {
            nbDoor = 5;
            annee = 1900;
            marque = "Unknow";
        }
        public Car(int nbDoor, int annee, string marque)
        {
            this.nbDoor = nbDoor;
            this.annee = annee;
            this.marque = marque;
        }
        // Method
        public void SetParameter(int nbDoor, int annee, string marque)
        {
            this.nbDoor = nbDoor;
            this.annee = annee;
            this.marque = marque;
        }
    }
    class Program
    {
        static void Main()
        {
            // Call the constructor that has no parameters.
            Car car1 = new Car();
            Console.WriteLine("Car 1, marque : " + car1.marque + " année : " + car1.annee + " nombre de portes :" + car1.nbDoor);
            car1.SetParameter(5, 2010, "Audi");
            Console.WriteLine("Car 1, marque : " + car1.marque + " année : " + car1.annee + " nombre de portes :" + car1.nbDoor);

            // Call the constructor that has one parameter.
            Car car2 = new Car(3, 2015, "BMW");
            Console.WriteLine("Car 2, marque : " + car2.marque + " année : " + car2.annee + " nombre de portes :" + car2.nbDoor);
            Console.ReadLine();
        }
    }

=========
Structure
=========

Les classes et les structures sont très similaire, les structures sont par contre de **type valeur**, la donnée de l'object est donc directement accessible. Il faut savoir aussi que les structures **ne prennent pas en compte l'héritage**. L'utilité des structures se trouvent dans le gain de perfomance lors de la manipulation des objects avec peu de variables.

using System;

.. code-block:: csharp

    public struct Car
    {
        public int nbDoor { get; set; }
        public int annee { get; set; }
        public string marque { get; set; }
        public Car(int nbDoor, int annee, string marque)
        {
            this.nbDoor = nbDoor;
            this.annee = annee;
            this.marque = marque;
        }
    }
    class Program
    {
        static void Main()
        {
            // Call the constructor that has no parameters.
            Car car = new Car(5, 2100, "Tesla");
            Console.WriteLine("Car 1, marque : " + car.marque + " année : " + car.annee + " nombre de portes :" + car.nbDoor);
            Console.ReadLine();
        }
    }

=================
Creation de types
=================

.. important::
    Petit rappel, il existe deux sortes de types en C#: Le type valeur et le type référence.
    pour stocker les données, la CLR dispose de deux zones de mémoire: la pile et le tas.

La pile fonctionne comme ceci: dernier entré, premier sortie. Cette zone de mémoire va stocker les variables du programmes.

Le tas est une zone réservée au stockage des Objets réutilisables. La CLR controlle automatiquement les entrées et les sorties de cette zone.


Type Valeur
-----------

Lors de sa durée de vie, le type valeur est affecté dans un espace de mémoire dans la pile.Lorsque que la variable est hors de porté, il est automatiquement détruit dans la zone mémoire de la pile.

.. important::
    Le type Valeur contient une valeur réel, donc il est impossible qu'elle soit `null`!

Les types valeurs sont:

.. code-block:: csharp

    Public Class ExempleController {
        enum Day { Sat, Sun, Mon, Tue, Wed, Thu, Fri = 18 };
        // L'enum ne prend que des valeurs byte, sbyte, short, ushort, int, uint, long ou ulong.
        struct Voiture
        {
            private string couleur;
            private bool allumer;
            public Voiture(string couleur = "bleu", bool allumer = false)
            {
                this.couleur = couleur;
                this.allumer = allumer;
            }
            public void ChangeCouleur(string couleur)
            {
                this.couleur = couleur;
            }
            public string GetCouleur()
            {
                return this.couleur;
            }
            public void AllumerVoiture()
            {
                this.allumer = true;
            }
            public void EteindreVoiture()
            {
                this.allumer = false;
            }
        }
        public ExempleController(){
            int i = 1;
            bool b = false;
            char c = 'c';
            var test = Day.Sat; // 0
            test = Day.Sun; //1
            test = Day.Fri; // 18
            Voiture maVoiture = new Voiture(  couleur: "vert");
            maVoiture.AllumerVoiture();
        }
    }

.. important::
    Petit rappel, la copie d'un `struct` dans un autre object identique, il copie juste les données et non le pointeur!

Type référence
--------------

Les données réeles du type sont stockés dans le tas et, un pointeur vers ces données est stockés dans la pile. Lors de l'utilisation d'une variable de type référence, c'est le pointeur qui est passé. Lorsque la variable est hors de portée, c'est le pointeur qui est libéré, et lorsqu'il n'y a plus de pointeur, l'object devient récupérable par le **garbage collector**.

Le type référence stocke les données dans le tas et un pointeur est sotcké dans les références (donc dans la pile).
Quand nous utilisons une variable de type référence, nous utilisons tout le temps l'adresse du pointeur.
Si la variable est hors de portée, elle réagit comme un type valeur, l'espace en mémoire de la pile est libéré mais cependant pas l'object, ce dernier restera dans le tas.

.. important::
    Les classes, les interfaces et les délégués représentent des types référence et la manipulation des objects créés à partir de ces types se fait indirectement via une référence vers leur espace mémoire alloué. Une référence doit être instancié grâce au mot-clé `new`.

.. code-block:: csharp

    using System;
    using System.Collections;

    namespace Révision
    {
        public class ExempleReference
        {
            public bool B;// bool est un type valeur!
            public string s = "je suis un type référence!"
        }
        class Program
        {
            static void Main(string[] args)
            {
                ArrayList tab1 = new ArrayList();
                ArrayList tab2 = new ArrayList(1); // Taille max du tableau
                ExempleReference c1 = new ExempleReference {B = true};
                ExempleReference c2 = c1;// le pointeur de c1 est copier dans c2
                c1.B = false;
                Console.WriteLine(c2.B); // false
            }
        }
    }

Les types génériques
--------------------

.. important::
    Le **Type Générique** n'a rien a voir avec le **Type Valeur** ou le **Type Référence**. Il n'est pas question de savoir où est les données ici.

Le **Type Générique** est ici utilisé pour l'implémentation d'une méthode ou d'une classe avec n'importe qu'elle type. 

Voici a quoi resemble une methode en type générique:

.. code-block:: csharp

    public static T max<T>(T a, T b) // Retourne la valeur maximale
    {
        return a > b ? a : b ;
    }
    int entier = max( 10 , 22 );
    double vmax = max( 3.14 , 1.618 );

Le compilateur traitera différament la méthode par rapport au type utilisé. 

.. important::
    Il va créer deux version dans lors de la compilation pour s'adapter à l'utilisation dans notre cas avec l'exemple ci-dessus.

    .. code-block:: csharp

        public static int max(int a, int b)
        public static double max(double a, double b)

Voici un exemple avec deux type générique:

.. code-block:: csharp

    public static void affiche<T,U>(T a, U b) // Affiche a et b
    {
        Console.WriteLine("A vaut {0}, et B vaut {1}", a, b);
    }
    affiche(10,3.1415926);


Voici un exemple pour un classe en type générique:

.. code-block:: csharp

    public class Arbre<T>
    {
        public T valeur;
        private Arbre<T> _gauche, _droite;
        public Arbre<T> ArbreGauche
        {
            get { return _gauche; }
        }
    }

    //Utilisation de la classe générique
    Arbre<int> ArbreDesEntiers = new Arbre<int>();
    ArbreDesEntiers.valeur = 100;


Voici un exemple avec une structure:

.. code-block:: csharp

    public struct Taille<T>
    {
        public T largeur, hauteur;
    }


.. important::
    pour faire une contrainte devons ajouter la clause **where**:

    .. code-block:: csharp

        public class TableauTriable<T>
        where T : IComparable
        {
            //...
        }


    .. code-block:: csharp
        public class TableauTriable<T>
        where T : struct // ou class par exemple
            //...
        {
        }

    on peut meme ajouter des contraintes sur le constructeur du type générique:

    .. code-block:: csharp

        public class TableauTriable<T>
        where T : new()   // T doit avoir un constructeur sans paramètre
        {
            public T Creer()
            {
                return new T(); // appel au constructeur de T
            }
        }


.. important::
    L'opérateur **default** permet de retourner une valeur dy type passé et lui retourner une donnée si elle n'est pas initialisé (**0** pour les nombres, **null** pour les types références):

    .. code-block:: csharp

        public T maxAbsolu(T a,T b)
        {
            if (a==b) return default(T);
            else return a>b ? a : b;
        }


Les constructeurs
-----------------

Nous les trouvons dans les **classes** ou les **structures**, elle porte le même nom que cette dernière. Il permet d'initialiser les membres de données du nouvel objet.

.. code-block:: csharp

    public class Taxi
    {
        public bool isInitialized;
        public Taxi()
        {
            isInitialized = true;
        }
    }

    class TestTaxi
    {
        static void Main()
        {
            Taxi t = new Taxi();
            Console.WriteLine(t.isInitialized);
        }
    }

.. important::
    - Un **constructeur** qui ne prend pas de paramètres est applée `constructeur par défaut`.
    
    - Si vous n’utilisez pas de modificateur d’accès avec le constructeur, le constructeur est toujours privé par défaut.

    - Les classes sans constructeurs sont automatiquement attribué a **public** par défaut lors de la compilation. Cependant, ce n'est pas le cas pour les **classes statique** qui ne dispose pas de constructeur.

    Pour empêcher qu'une classe soit instanciée, vous devez rendre le constructeur privé:

    .. code-block:: csharp

        class Exemple
        {
            private Exemple() { }
            public static double e = Math.E;
        }

    - Pour rappel, nous utilisons souvent un constructeur privé dans les classes qui contiennent uniquement des membres statiques. (Nous ne parlons pas de class static, cela reprend la logique d'une classe static sans en etre une!)

    - En règle générale, le `modificateur private` (le constructeur privé) est explicitement utilisé pour spécifier que la classe ne peut pas être instanciée.
    (Nous ne parlons pas de class static, cela reprend la logique d'une classe static sans en etre une!)

Si une classe a un ou plusieurs constructeurs privés, mais n’a aucun constructeur public, les autres classes (à l’exception des classes imbriquées) ne peuvent pas créer d’instances de cette classe.

.. code-block:: csharp

    public class Exemple{
        private Exemple() { }
        Exemple uniquementDansMaClasse = new Exemple(); // OK
    }

    public class Main {
        
        static void Main(){
            Exemple impossible = new Exemple(); // erreur
        }
    }

Le type imbriqué (class et struct)
----------------------------------

Le type imbriqué est comme ceci :

.. code-block:: csharp

    public class Container
    {
        public class Nested
        {
            private Container parent;
            public Nested()
            {
            }
            public Nested(Container parent)
            {
                this.parent = parent;
            }
        }
    }

n type imbriqué a accès à tous les membres qui sont accessibles à son type conteneur. Il peut accéder aux membres privés et protégés du type conteneur, y compris à tous les membres protégés hérités.

Dans la déclaration précédente, le nom complet de classe `Nested` est `Container.Nested`. Il s'agit du nom utilisé pour créer une instance de la classe imbriquée, comme suit :

.. code-block:: csharp

    Container.Nested nest = new Container.Nested();


Les méthodes d'extension
------------------------

Il s'agit d'ici de surcharger une classe pour étendre les fonctionnalités, voici un exemple avec ma méthode slugify:

.. code-block:: csharp

    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;

    namespace Exemple.Extensions
    {
        public static class StringExtension
        {
            public static string EncodeToUtf8(this string stringToEncode)
            {
                if (string.IsNullOrEmpty(stringToEncode))
                    return string.Empty;
                var bytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(stringToEncode);
                var utf8String = Encoding.UTF8.GetString(bytes);
                return utf8String;
            }

            public static string Slugify(this string inputString)
            {
                if (string.IsNullOrEmpty(inputString))
                    return string.Empty;
                var inputStringBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(inputString);
                var outputString = Encoding.UTF8.GetString(inputStringBytes).ToLower().Trim();
                return Regex.Replace(outputString, @"[^a-zA-Z0-9_]", "_", RegexOptions.Compiled).TrimEnd('_').TrimStart('_');
            }
        }

        Public class Main{

            static Main(){
                var test = "un jolie slug".Slugify();
            }
        }
    }

=========================================
Creation de paramètres optionnels et typé
=========================================

Voici un exemple de paramètre optionnel:

.. code-block:: csharp

    static class Exemple
    {
        public static string test(string test = "je suis pas null")
        {
            return test;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var e = Exemple.test(); // "je suis pas null"
            var u = Exemple.test("ok"); // "ok" 
        }
    }

ou

.. code-block:: csharp

    static class Exemple
    {
        public static string test(int i, string s = "je suis pas null", char c = 'c' )
        {
            return s + i.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var e = Exemple.test(1); // 1
            var u = Exemple.test(i: 1, s: "ok"); // "ok1" 
        }
    }

.. important::
    Il est recommandé de nommer les paramètres a l'appel de la methode!

=======================
Propriétés et indexeurs
=======================


Propriétés
----------

Voici une propriétés (qui sont en générale dans les classes ou les structures):

.. code-block:: csharp

    public int MaPropriétés;

nous pouvons lui dire qu'elle est accessible uniquement en lecture:

.. code-block:: csharp

    public int MaPropriétés { get; }

ou en écriture:

.. code-block:: csharp

    public int MaPropriétés { get; set; }


Nous pouvons aussi retrouver cette syntaxe:

.. code-block:: csharp

    private int _champ;
    
    public int SetChamp
    {
        set => _champ = value;
    }

    //ou
    public int Champ
    {
        get => _champ;
        set => _champ = value; // Nous pouvons aussi ajouter un traitement en plus ici
    }

.. important::
    Le mot clé value est une sorte de paramètre implicite interne à l'accesseur set, il contient la valeur effective qui est transmise à la propriété lors de l'accès en écriture.

Indexeurs
---------
Un indexeur est un membre de classe qui permet à un objet d'être indexé de la même manière qu'un tableau. La signature d'un indexeur doit être différente des signatures de tous les autres indexeurs déclarés dans la même classe. Les indexeurs et les propriétés sont très similaires de par leur concept, c'est pourquoi nous allons définir les indexeurs à partir des propriétés.

Tous les indexeurs sont représentés par l' opérateur [  ] . Les liens sur les propriétés ou les indexeurs du tableau ci-dessous renvoient directement au paragraphe associé.

+-------------------------------------------------------------+---------------------------------------------------------------------+
| Propriété                                                   | Indexeur                                                            |
+=============================================================+=====================================================================+
| Déclarée par son nom.                                       | Déclaré par le mot clef this.                                       |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Identifiée et utilisée par son nom.                         | Identifié par sa signature, utilisé par l'opérateur  [ ] .          |
|                                                             | L'opérateur [ ] doit être situé immédiatement après le nom de       |
|                                                             | l'objet.                                                            |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Peut être un membre de classe static ou un membre           | Ne peut pas être un membre static, est toujours un membre           |
| d'instance.                                                 | d'instance.                                                         |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| L'accesseur get correspond à une méthode sans paramètre.    | L'accesseur get correspond à une méthode pourvue de la même liste   |
|                                                             | de paramètres formels que l'indexeur.                               |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| L'accesseur set correspond à une méthode avec un seul       | L'accesseur set correspond à une méthode  pourvue de la même        |
| paramètre implicite value.                                  | liste de paramètres formels que l'indexeur plus le paramètre        |
|                                                             | implicite value.                                                    |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Une propriété Prop héritée est accessible par la            | Un indexeur Prop hérité est accessible par la syntaxe base.[  ]     |
| syntaxe base.Prop                                           |                                                                     |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Les propriétés peuvent être à liaison statique, à liaison   | Les indexeurs peuvent être à liaison statique, à liaison dynamique  |
| dynamique, masquées ou redéfinies.                          | ,masqués ou redéfinis.                                              |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Les propriétés peuvent être abstraites.                     | Les indexeurs peuvent être abstraits.                               |
+-------------------------------------------------------------+---------------------------------------------------------------------+
| Les propriétés peuvent être déclarées dans une interface.   | Les indexeurs peuvent être déclarés dans une interface.             |
+-------------------------------------------------------------+---------------------------------------------------------------------+

.. code-block:: csharp
   
    class TempRecord
    {
        // Array of temperature values
        private float[] temps = new float[10] { 56.2F, 56.7F, 56.5F, 56.9F, 58.8F,
                                                61.3F, 65.9F, 62.1F, 59.2F, 57.5F };

        // To enable client code to validate input 
        // when accessing your indexer.
        public int Length
        {
            get { return temps.Length; }
        }
        // Indexer declaration.
        // If index is out of range, the temps array will throw the exception.
        public float this[int index]
        {
            get
            {
            var result = (temps[index] - 32) / 1.8F;
            return result ;
            }

            set
            {
                temps[index] = value;
            }
        }
    }

    class MainClass
    {
        static void Main()
        {
            TempRecord tempRecord = new TempRecord();
            // Use the indexer's set accessor
            tempRecord[3] = 58.3F;
            tempRecord[5] = 60.1F;

            // Use the indexer's get accessor
            for (int i = 0; i < 10; i++)
            {
                System.Console.WriteLine("Element #{0} On celcius = {1}", i, tempRecord[i]);
            }
            System.Console.WriteLine("Lenght of array :" + tempRecord.Length);
            System.Console.ReadLine();

        }
    }

Abstract
--------

Le modificateur abstract indique que l’élément en cours de modification a une implémentation manquante ou incomplète. Le modificateur abstract peut être utilisé avec des classes, des méthodes, des propriétés, des indexeurs et des événements. Dans une déclaration de classe, utilisez le modificateur abstract pour indiquer qu’une classe doit uniquement servir de classe de base pour d’autres classes. Les membres définis comme abstraits, ou inclus dans une classe abstraite, doivent être implémentés par des classes dérivées de la classe abstraite.

.. code-block:: csharp

    abstract class ShapesClass
    {
        abstract public int Area();
    }
    class Square : ShapesClass
    {
        int side = 0;

        public Square(int n)
        {
            side = n;
        }
        // Area method is required to avoid
        // a compile-time error.
        public override int Area()
        {
            return side * side;
        }

        static void Main() 
        {
            Square sq = new Square(12);
            Console.WriteLine("Area of the square = {0}", sq.Area());
        }

        interface I
        {
            void M();
        }
        abstract class C : I
        {
            public abstract void M();
        }

    }
    // Output: Area of the square = 144

.. important::
    La différence entre la méthode **abstract** et une interface est que la methode abstract donne une signiature d'une partie des methodes ou des propriétés que nous devons utilisé dans la class. l'interface nous donne aussi c'est information mais pas exactement la meme chose. 

    - Nous pouvons donner une portée différente que public avec l'abstract.
    - Une classe abstract ne peut pas etre instancié.
    - Une classe abstract peut contenir des methodes et accesseur abstract.
    - Il n’est pas possible de modifier une classe abstraite à l’aide du modificateur sealed, car les deux modificateurs ont des significations opposées. Le modificateur sealed empêche qu’une classe soit héritée et le modificateur abstract exige qu’une classe soit héritée.
    - une classe non abstraite dérivée d’une classe abstraite doit inclure des implémentations réelles de tous les accesseurs et méthodes abstraits hérités.

    Dans une déclaration de méthode ou de propriété, utilisez le modificateur abstract pour indiquer que la méthode ou la propriété ne contient pas d’implémentation.

    - Les méthodes abstraites présentent les caractéristiques suivantes :

    - Une méthode abstraite est implicitement une méthode virtuelle.

    - Les déclarations de méthodes abstraites sont autorisées uniquement dans les classes abstraites.

    - Comme une déclaration de méthode abstraite ne fournit pas d’implémentation réelle, il n’y a pas de corps de méthode ; la déclaration de méthode se termine simplement par un point-virgule, et la signature n’est pas suivie d’accolades ({ }). Exemple :

    .. code-block:: csharp

        public abstract void MyMethod();

     L’implémentation est fournie par une méthode de substitution (override), qui est membre d’une classe non abstraite.

    - L’utilisation des modificateurs static ou virtual dans une déclaration de méthode abstraite serait une erreur.

     Les propriétés abstraites se comportent comme les méthodes abstraites, à l’exception des différences dans la syntaxe de déclaration et d’appel.

    - L’utilisation du modificateur abstract sur une propriété statique serait une erreur.

    - Une propriété abstraite héritée peut être substituée dans une classe dérivée en incluant une déclaration de propriété qui utilise le modificateur override.

Dans cet exemple, la classe DerivedClass est dérivée de la classe abstraite BaseClass. La classe abstraite contient une méthode abstraite, AbstractMethod, et deux propriétés abstraites, X et Y.

.. code-block:: csharp

    abstract class BaseClass   // Abstract class
    {
        protected int _x = 100;
        protected int _y = 150;
        public abstract void AbstractMethod();   // Abstract method
        public abstract int X    { get; }
        public abstract int Y    { get; }
    }
    class DerivedClass : BaseClass
    {
        public override void AbstractMethod()
        {
            _x++;
            _y++;
        }
        public override int X   // overriding property
        {
            get
            {
                return _x + 10;
            }
        }
        public override int Y   // overriding property
        {
            get
            {
                return _y + 10;
            }
        }
        static void Main()
        {
            DerivedClass o = new DerivedClass();
            o.AbstractMethod();
            Console.WriteLine("x = {0}, y = {1}", o.X, o.Y);
        }
    }
    // Output: x = 111, y = 161

