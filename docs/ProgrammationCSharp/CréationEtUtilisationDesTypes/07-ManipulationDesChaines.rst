########################
Manipulation des chaînes
########################

Manipulation des chaînes à l'aide des classes StringBuilder, StringWriter et StringReader ; recherche de chaînes ; énumération des méthodes de chaîne ; formatage des chaînes ; utilisation de l'interpolation de chaîne

===========================================================
Manipulation des chaînes à l'aide des classes StringBuilder
===========================================================

Le type String est immuable. Chaque fois que vous utilisez une des méthodes de System.String, vous créez un nouvel objet string en mémoire, ce qui requiert une nouvelle allocation d’espace pour ce nouvel objet. Il est donc préférable d'utiliser la classe **StringBuilder** lorsque la chaine de string sera souvent modifié.

Dans l'exemple ci-dessous on déclare un StringBuilder basique

.. code-block:: csharp

    StringBuilder MyStringBuilder = new StringBuilder("Hello World!");

Mais il est aussi possible d'utilisé une surchage de la classe StringBuilder ce qui permet de définir par exemple une capacité de base, qui pourra être augmenter jusqu'a une capacité maximal. Dans l'exemple ci dessous, on définit la capacité de base à 5 et la capacité maximun de 10. Lorsque la chaine dépasse les 5 caractères, le stringBuilder augmente sa capacité jusqu'a 10, son maximun et, si on essaye de dépasser la capacité de 10, on obtient une exception de type "System.ArgumentOutOfRangeException" sur la ligne de l'insertion de "test3".

.. code-block:: csharp

        StringBuilder myStringBuilder = new StringBuilder(5,10);
        myStringBuilder.Insert(0, "test1");
        Console.WriteLine(myStringBuilder);
        myStringBuilder.Insert(0, "test2");
        Console.WriteLine(myStringBuilder).
        myStringBuilder.Insert(0, "test3");
        Console.WriteLine(myStringBuilder);
