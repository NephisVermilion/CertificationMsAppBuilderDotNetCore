#####################
Consommation de types
#####################

Utilisation des fonctions Box ou Unbox pour convertir entre les types de valeur;
Distribuer les types;
Convertir les types;
Gérer les types dynamiques;
Assurer l'interopérabilité avec le code qui accède aux API COM;

=======================================================================================
Utilisation des fonctions Box et Unbox pour réaliser la convertion des types de valeurs
=======================================================================================

Boxing
------


Le concept de Boxing pour un type valeur est sa conversion en un type object ou dans un type interface qui elle même est implémenté sur ce type valeur.

Dans le cas du Boxing la conversion est implicite: 
int myInteger = 123;
// myBoxed is the box value of myInteger
object myBoxed = myInteger;

Unboxing
--------

Le concept de Unboxing est donc de repasser un object dans un autre type valeur

.. code-block:: csharp

    myBoxed = 123;
    myInteger = (int)myBoxed;  // unboxing

Dans l'exemple ci-dessus, l'éxécution renverra une InvalidCastException, un object boxer d'un int ne peut être unboxer en short, il faut utiliser une conversion implicite.

.. code-block:: csharp

    using System;

    class TestUnboxing
    {
        static void Main()
        {
            int i = 123;
            object o = i;  // implicit boxing

            try
            {
                int j = (short)o;  // attempt to unbox

                System.Console.WriteLine("Unboxing OK.");
            }
            catch (System.InvalidCastException e)
            {
                System.Console.WriteLine("{0} Error: Incorrect unboxing.", e.Message);
            }
            Console.ReadLine();
        }
    }

===================
Convertir les types
===================

En C# les valeurs sont typés statiquement au moment de la compilation. Ainsi, quand une variable est déclarée, elle ne peut plus être redéclarée ou utilisée pour stocker des valeurs d’un autre type, sauf si ce type peut être converti au type de la variable. Par exemple, il est impossible de convertir un entier en chaîne de caracètère ; Après avoir déclaré i comme entier, vous ne pouvez donc pas lui assigner la chaîne « Hello » comme le montre le code suivant.

.. code-block:: csharp

    int myInteger;  
    myInteger = "Hello World !"; // Error: "Cannot implicitly convert type 'string' to 'int'"

.. important::
    Conversions implicites : aucune syntaxe spéciale n’est requise, car la conversion est de type sécurisé et les données ne sont pas perdues. Citons par exemple les conversions de types intégraux en d’autres plus importants, et les conversions de classes dérivées en classes de base. 

    Conversions explicites (casts) : les conversions explicites nécessitent un opérateur de cast. Un cast est exigé quand les informations peuvent être perdues durant la conversion, ou quand la conversion peut échouer pour d’autres raisons. Exemples classiques : conversion numérique en type qui a moins de précision ou une plus petite plage, et conversion d’une instance de classe de base en classe dérivée.

    Conversions définies par l’utilisateur : les conversions définies par l’utilisateur sont effectuées par des méthodes spéciales que vous pouvez définir pour permettre des conversions explicites ou implicites entre des types personnalisés qui n’ont pas de relation classe de base/classe dérivée.

    Conversions avec les classes d’assistance : pour effectuer une conversion entre des types non compatibles, tels que des entiers et des objets System.DateTime ou des chaînes hexadécimales et des tableaux d’octets, vous pouvez utiliser la classe System.BitConverter, la classe System.Convert et les méthodes Parse des types numériques intégrés, comme Int32.Parse.


Utilisez des casts sans risque à l'aide des opérateurs as et is
---------------------------------------------------------------

Vous pouvez aussi utilisez les opérateurs **as** et **is** qui permettent de vérifier qu'une convertion de type cast pourra être réalisé sans lever une exception de type "InvalidCastException".

Pour le cas du "as", soit la valeur est converti dans le type demandé, soit elle renvoie un null.
Pour le cas du "is", c'est simplement un test qui renvoie un boolean sur la possibilité ou pas de convertir la valeur dans le type demandé.

Exemple du **as**

.. code-block:: csharp

    void testAs(object myObject)
    {
        Animal myAnimal = myObject as Animal;
        if (myAnimal != null)
        {
            Console.WriteLine(myAnimal.ToString());
        }
        else
        {
            Console.WriteLine("{0} is not an Animal", myObject.GetType().Name);
        }
    }

Exemple du **is**

.. code-block:: csharp

    void testIs(Animal myAnimal)
    {
        if (myAnimal is Tiger)
        {
            Tiger myTiger = (Tiger)myAnimal;
            myTiger.Eat();
        }
    }

===================
Gestion des types dinamiques
===================

Le type dynamic permet pour les instructions dans lesquelles il se produit de **contourner la vérification de type** au moment de la compilation. Les opéarations sont alors résolue au moment de l'éxécution.

Le type dynamic se comporte comme le type object dans la plupart des cas. Toutefois, les instructions qui contiennent des expressions de type dynamic ne sont pas résolues et leur type n’est pas vérifié par le compilateur. Le compilateur empaquète des informations sur l'insctuction, qui sont ensuite utilisées pour évaluer l'opération au moment de l’exécution. Dans le cadre du processus, les variables de type dynamic sont compilées dans des variables de type object. Ainsi, le type dynamic existe seulement au moment de la compilation, et non au moment de l’exécution.

Pour l'exemple ci-dessous par exemple, le compilateur bloque la compilation sur la ligne **dyn=dyn+3**

.. code-block:: csharp

    using System;
    class Program
    {
        static void Main(string[] args)
        {
            dynamic dyn = 1;
            object obj = 1;
            obj = obj + 3;
            dyn = dyn + 3;
            Console.WriteLine(dyn);
            Console.WriteLine(obj);
            Console.ReadLine();
        }
    }

Si par contre on commente les lignes sur le type object, la compilation à bien lieu, et le retour est 4.

.. code-block:: csharp

    using System;

    class Program
    {
        static void Main(string[] args)
        {
            dynamic dyn = 1;
            //object obj = 1;
            //obj = obj + 3;
            dyn = dyn + 3;
            Console.WriteLine(dyn);
            //Console.WriteLine(obj);
            Console.ReadLine();
        }
    }


==============================================================
Assurer l'interopérabilité avec le code qui accède aux API COM
==============================================================

//TODO 

**Informations non trouvé sur ce sujet**


