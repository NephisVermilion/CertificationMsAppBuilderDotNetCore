#######################
Utilisation des données
#######################

Récupération des données à partir d'une base de données ; mise à jour des données dans une base de données ; consommation des données JSON et XML ; récupération des données à l'aide des services Web
