##############################################
Mise en oeuvre de l'accès aux données (25-30%)
##############################################

.. toctree::
   :maxdepth: 4
   :numbered:

   01-ExécutionDesOpérationDES
   02-UtilisationDesDonnées
   03-InterrogationEtManipulationDesDonnéesEtDesObjetsALAideDeLIMQ
   04-SérialisationEtDésérialisationDesDonnées
   05-StockageDesDonnéesDansLesCollectionsEtExtractionDesDonnéesDeCellesCi
   