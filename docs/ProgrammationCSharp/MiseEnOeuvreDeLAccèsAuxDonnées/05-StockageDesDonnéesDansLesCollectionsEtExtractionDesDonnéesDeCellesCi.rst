################################################################################
Stockage des données dans les collections et extraction des données de celles-ci
################################################################################

Stockage et récupération des données à l'aide de dictionnaires, tableaux, listes, ensembles et files d'attente ; choix d'un type de collection ; initialisation d'une collection ; ajout d'éléments à une collection et retrait d'éléments de celle-ci ; utilisation des collections typées par rapport aux collections non typées ; mise en œuvre de collections personnalisées ; mise en œuvre d'interfaces de collection