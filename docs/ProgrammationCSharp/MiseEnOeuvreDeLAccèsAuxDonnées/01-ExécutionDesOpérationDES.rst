##############################
Exécution des opérations d'E/S
##############################

Lecture et écriture des fichiers et des flux ; lecture et écriture à partir du réseau à l'aide de classes dans l'espace de noms System.Net ; mise en œuvre des opérations d'E/S asynchrones