########################################################################
Interrogation et manipulation des données et des objets à l'aide de LINQ
########################################################################

Interrogation des données à l'aide d'opérateurs (projection, rejoindre, grouper, prendre, sauter, agrégat) ; création de requêtes LINQ basées sur une méthode ; interrogation des données à l'aide d'une syntaxe de compréhension de requête ; sélection des données à l'aide de types anonymes ; forçage de l'exécution d'une requête ; lecture, filtrage, création et modification des structures de données à l'aide de LINQ to XML
