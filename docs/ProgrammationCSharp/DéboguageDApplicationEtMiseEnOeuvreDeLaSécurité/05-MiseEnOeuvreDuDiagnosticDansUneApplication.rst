################################################
Mise en œuvre du diagnostic dans une application
################################################

Mise en œuvre de la journalisation et du traçage ; profilage des applications ; création et analyse des compteurs de performances ; écriture dans le journal des événements
