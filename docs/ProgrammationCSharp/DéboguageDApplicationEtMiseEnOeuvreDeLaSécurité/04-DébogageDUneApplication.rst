##########################
Débogage d'une application
##########################

Créer et gérer des directives de préprocesseur ; choisir un type de construction approprié ; gérer les fichiers de base de données du programme (symboles de débogage)
