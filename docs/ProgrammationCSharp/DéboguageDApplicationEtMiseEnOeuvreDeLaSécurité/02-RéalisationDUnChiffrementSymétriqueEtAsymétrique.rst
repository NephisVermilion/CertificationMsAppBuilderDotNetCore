######################################################
Réalisation d'un chiffrement symétrique et asymétrique
######################################################

Choix d'un algorithme de chiffrement approprié ; gestion et création des certificats ; mise en œuvre de la gestion des clés ; mise en œuvre de l'espace de noms System.Security ; données de hachage ; chiffrement des flux