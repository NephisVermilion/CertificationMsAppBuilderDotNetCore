#######################
Gestion des assemblages
#######################

Contrôle des versions des assemblages ; signature des assemblages à l'aide de noms forts ; mise en œuvre de l'hébergement côte à côte ; mise d'un assemblage dans le Global Assembly Cache ; création d'un assemblage WinMD
